﻿using System.IO;
using Newtonsoft.Json;

namespace Configuration
{

    /// <summary>
    /// Global configuration static as the Environment/Configuration globals of .Net framework are not usable in core
    /// </summary>
    public static class Configuration
    {
        public static Config Global { get; set; }

        public static Config ReadConfig()
        {
            var text = File.ReadAllText("config.json");
            return JsonConvert.DeserializeObject<Config>(text);
        }

        /// <summary>
        /// n.b. still to be removed. Here to ease creation of the config
        /// </summary>
        public static void UpdateConfig()
        {
            var config = new Config()
            {
                AbcMagicString = "_1HzXw",
                WebsiteUrl = "http://smallest.wang",
                BbcMagicString = "ssrcss - uf6wea - RichTextComponentWrapper e1xue1i83",
                TermTaxonomyId = 8,
                AbcUrl = "https://www.abc.net.au/news/feed/4535882/rss.xml",
                BbcUrl = "http://feeds.bbci.co.uk/news/world/rss.xml",
                MySqlConnectionString = @"Server=smallest.wang;Database=wordpress_db;Uid=rupert;Pwd=loldongs;",
                SqliteConnectionString = @"Data Source = ..\..\..\db.sqlite3"
            };
            var text = JsonConvert.SerializeObject(config, Formatting.Indented);
            File.WriteAllText("config.json", text);
        }
    }
}
