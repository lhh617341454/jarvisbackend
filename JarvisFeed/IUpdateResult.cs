﻿using System;
using System.Collections.Generic;
using System.Text;
using Database;

namespace JarvisFeed
{
    interface IUpdateResult
    {
        public IEnumerable<StoryDto> ReturnedStories { get; }

    }
}
