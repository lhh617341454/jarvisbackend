﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataModels;
using LinqToDB;
using LinqToDB.Common;
using LinqToDB.Configuration;
using LinqToDB.Data;
using LinqToDB.Tools;
using WpDataModels;

namespace Database
{
    public static class WordpressDb
    {


        public static void AddPosts(IEnumerable<StoryDto> stories)
        {
            using var db = new WordpressDbDB("MySql");
            var uploadedTitles = db.WpPosts.Select(x => x.PostTitle).ToList();
            foreach (var story in stories.Where(x=>x.Title.NotIn(uploadedTitles)))
            {
                 AddPost(story);
            }
        }

        /// <summary>
        /// Retrieve ALL posts. At the moment used only in testing
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<WpPost> RetrievePosts()
        {
            using var db = new WordpressDbDB("MySql");
            return db.WpPosts;
        }

        public static void AddPost(StoryDto story)
        {
            using var db = new WordpressDbDB("MySql");
            // get the next id
            var postId = db.WpPosts.OrderByDescending(x => x.ID).FirstOrDefault().ID + 1;
            db.Insert(new WpPost()
            {
                CommentCount = 0,
                CommentStatus = "open",
                Guid = Configuration.Configuration.Global.WebsiteUrl + @"/?p=" + postId,
                ID = postId,
                MenuOrder = 0,
                Pinged = String.Empty,
                PingStatus = String.Empty,
                //n.b. probably has to be configured per instance
                PostAuthor = 1,
                PostContent = story.StoryTextLink,
                PostContentFiltered = "",
                PostDate = DateTime.Now,
                PostExcerpt = "",
                PostDateGmt = DateTime.Now,
                PostParent = 0,
                PostTitle = story.Title,
                PostStatus = "draft",
                PostPassword = "",
                PostName = story.Tag + " story  at " + DateTime.UtcNow,
                ToPing = "",
                PostType = "post",
                PostMimeType = ""
            });

            db.Insert(new WpTermRelationship()
            {
                ObjectId = postId,
                TermOrder = 0,
                // todo encode in config - corresponds to wp_terms where name = PublishQ
                TermTaxonomyId = Configuration.Configuration.Global.TermTaxonomyId
            });

        }
    }
}
